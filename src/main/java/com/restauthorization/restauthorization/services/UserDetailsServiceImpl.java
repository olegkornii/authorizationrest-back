package com.restauthorization.restauthorization.services;

import com.restauthorization.restauthorization.model.User;
import com.restauthorization.restauthorization.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    /**
     *Create a UserDetails from a String-based username and is usually used by
     *
     * @param username
     * @return UserDetail
     * @throws UsernameNotFoundException
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username){

        User user = userRepository.findByUsername(username).orElseThrow(
                () -> new UsernameNotFoundException("User Not Found with -> username or email : " + username));
//        logger.info("User Not Found with -> username or email : " + username);

        return UserPrinciple.build(user);
    }
}
