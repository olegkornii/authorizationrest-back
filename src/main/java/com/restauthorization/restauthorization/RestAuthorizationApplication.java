package com.restauthorization.restauthorization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestAuthorizationApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestAuthorizationApplication.class, args);
    }

}

