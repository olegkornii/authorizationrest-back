package com.restauthorization.restauthorization.controllers;

import com.restauthorization.restauthorization.model.User;
import com.restauthorization.restauthorization.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController

public class AdminAPIs {
    @Autowired
    UserRepository userRepository;

    @GetMapping("/api/test/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> adminAccess() {
        return userRepository.findAll();
    }

    @DeleteMapping(path = {"/api/test/admin/{id}"})
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable("id") int id) {
        Optional<User> userValue = userRepository.findById((long) id);

        if (userValue.isPresent()) {
            User user = userValue.get();
            userRepository.delete(user);
        }
    }
}
