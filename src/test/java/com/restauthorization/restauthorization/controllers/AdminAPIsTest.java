package com.restauthorization.restauthorization.controllers;

import com.restauthorization.restauthorization.model.User;
import com.restauthorization.restauthorization.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;


public class AdminAPIsTest {

    @InjectMocks
    AdminAPIs adminAPIs;
    @Mock
    private UserRepository userRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void adminAccess() {
        List<User> userList = new ArrayList<>();
        when(userRepository.findAll()).thenReturn(userList);
        assertNotNull(adminAPIs.adminAccess());

    }

    @Test
    public void delete() {
        User user = new User();
        Optional<User> userOptional = Optional.of(user);
        when(userRepository.findById(anyLong())).thenReturn(userOptional);
        adminAPIs.delete(10);
        adminAPIs.delete(anyInt());

    }


}