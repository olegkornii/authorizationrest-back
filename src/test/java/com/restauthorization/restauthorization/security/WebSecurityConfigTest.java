package com.restauthorization.restauthorization.security;

import com.restauthorization.restauthorization.security.jwt.JwtAuthTokenFilter;
import com.restauthorization.restauthorization.services.UserDetailsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.Assert.assertTrue;

public class WebSecurityConfigTest {
    @InjectMocks
    WebSecurityConfig webSecurityConfig;

    @Mock
    UserDetailsServiceImpl userDetailsService;

    @Before
    public void set_up() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void authenticationJwtTokenFilter() {
        assertTrue(webSecurityConfig.authenticationJwtTokenFilter() instanceof JwtAuthTokenFilter);
    }

    @Test
    public void configure() throws Exception {

    }

    @Test
    public void authenticationManagerBean() {

    }

    @Test
    public void passwordEncoder() {
        assertTrue(webSecurityConfig.passwordEncoder() instanceof BCryptPasswordEncoder);
    }


}