package com.restauthorization.restauthorization.security.jwt;

import org.junit.Test;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.mock;

public class JwtAuthEntryPointTest {
    JwtAuthEntryPoint jwtAuthEntryPoint = new JwtAuthEntryPoint();

    @Test
    public void commence() throws IOException, ServletException {
        AuthenticationException authenticationException = mock(AuthenticationException.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        jwtAuthEntryPoint.commence(request, response, authenticationException);

    }
}