package com.restauthorization.restauthorization.security.jwt;

import com.restauthorization.restauthorization.services.UserPrinciple;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.Test;
import org.springframework.security.core.Authentication;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JwtProviderTest {
    String jwtSecret = "jwtGrokonezSecretKey";
    int jwtExpiration = 86400;
    JwtProvider jwtProvider = new JwtProvider();

    @Test
    public void generateJwtToken() {

        UserPrinciple userPrincipal = mock(UserPrinciple.class);
        when(userPrincipal.getUsername()).thenReturn("Oleg");
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(userPrincipal);


//        assertEquals(jwtProvider.generateJwtToken(authentication), Jwts.builder()
//                .setSubject((userPrincipal.getUsername()))
//                .setIssuedAt(new Date())
//                .setExpiration(new Date((new Date()).getTime() + jwtExpiration * 1000))
//                .signWith(SignatureAlgorithm.HS512, jwtSecret)
//                .compact());
    }

    @Test
    public void validateJwtToken() {
        UserPrinciple userPrincipal = mock(UserPrinciple.class);
        when(userPrincipal.getUsername()).thenReturn("Oleg");
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(userPrincipal);
        assertTrue(jwtProvider.validateJwtToken(jwtProvider.generateJwtToken(authentication)));
    }


    @Test
    public void getUserNameFromJwtToken() {
        UserPrinciple userPrincipal = mock(UserPrinciple.class);
        when(userPrincipal.getUsername()).thenReturn("Oleg");
        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(userPrincipal);
        assertEquals("Oleg", jwtProvider.getUserNameFromJwtToken(jwtProvider.generateJwtToken(authentication)));

    }
}