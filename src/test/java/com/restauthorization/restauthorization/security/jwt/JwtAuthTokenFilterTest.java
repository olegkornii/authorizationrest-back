package com.restauthorization.restauthorization.security.jwt;

import com.restauthorization.restauthorization.model.Role;
import com.restauthorization.restauthorization.model.RoleName;
import com.restauthorization.restauthorization.model.User;
import com.restauthorization.restauthorization.services.UserDetailsServiceImpl;
import com.restauthorization.restauthorization.services.UserPrinciple;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JwtAuthTokenFilterTest {

    @InjectMocks
    JwtAuthTokenFilter jwtAuthTokenFilter;

    @Mock
    JwtProvider jwtProvider;

    @Mock
    UserDetailsServiceImpl userDetailsService;

    User user = new User();
    Role role = new Role();
    Set<Role> roleSet = new HashSet<>();
    UserDetails userDetails;

    @Before
    public void set_up() {
        MockitoAnnotations.initMocks(this);
        role.setName(RoleName.ROLE_USER);
        role.setId(1L);

        roleSet.add(role);
        user.setId(1L);
        user.setPassword("12345678");
        user.setEmail("oleg@gmail.com");
        user.setName("Oleg");
        user.setUsername("Oleg");
        user.setRoles(roleSet);


        userDetails = UserPrinciple.build(user);
    }

    @Test
    public void doFilterInternal_jwt_null() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(userDetails);

        jwtAuthTokenFilter.doFilterInternal(request, response, filterChain);

    }

    private String getJwt(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");

        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.replace("Bearer ", "");
        }

        return null;
    }

}