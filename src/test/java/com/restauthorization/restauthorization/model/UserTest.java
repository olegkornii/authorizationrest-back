package com.restauthorization.restauthorization.model;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class UserTest {


    @Test
    public void getId() {

        User user = new User();
        user.setId(10L);

        assertEquals(10L, (long) user.getId());
    }

    @Test
    public void setId() {
        User user = new User();
        user.setId(10L);

        assertEquals(10L, (long) user.getId());
    }

    @Test
    public void getUsername() {
        User user = new User();
        user.setUsername("Oleg_Kornii");
        assertEquals("Oleg_Kornii", user.getUsername());
    }

    @Test
    public void setUsername() {
        User user = new User();
        user.setUsername("Oleg_Kornii");
        assertEquals("Oleg_Kornii", user.getUsername());
    }

    @Test
    public void getName() {
        User user = new User();
        user.setName("Oleg");
        assertEquals("Oleg", user.getName());
    }

    @Test
    public void setName() {
        User user = new User();
        user.setName("Oleg");
        assertEquals("Oleg", user.getName());
    }

    @Test
    public void getEmail() {
        User user = new User();
        user.setEmail("olegnatikeros@gmail.com");
        assertEquals("olegnatikeros@gmail.com", user.getEmail());

    }

    @Test
    public void setEmail() {
        User user = new User();
        user.setEmail("olegnatikeros@gmail.com");
        assertEquals("olegnatikeros@gmail.com", user.getEmail());
    }

    @Test
    public void getPassword() {
        User user = new User();
        user.setPassword("12345678");
        assertEquals("12345678", user.getPassword());
    }

    @Test
    public void setPassword() {
        User user = new User();
        user.setPassword("12345678");
        assertEquals("12345678", user.getPassword());
    }

    @Test
    public void getRoles() {
        Role role = new Role();
        role.setName(RoleName.ROLE_ADMIN);
        User user = new User();
        Set<Role> roles = new HashSet<>();
        user.setRoles(roles);
        assertEquals(roles, user.getRoles());
    }

    @Test
    public void setRoles() {
        Role role = new Role();
        role.setName(RoleName.ROLE_ADMIN);
        User user = new User();
        Set<Role> roles = new HashSet<>();
        user.setRoles(roles);
        assertEquals(roles, user.getRoles());
    }
}