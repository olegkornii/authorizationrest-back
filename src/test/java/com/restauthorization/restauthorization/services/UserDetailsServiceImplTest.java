package com.restauthorization.restauthorization.services;

import com.restauthorization.restauthorization.model.Role;
import com.restauthorization.restauthorization.model.RoleName;
import com.restauthorization.restauthorization.model.User;
import com.restauthorization.restauthorization.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class UserDetailsServiceImplTest {
    @InjectMocks
    UserDetailsServiceImpl userDetailsService;

    @Mock
    UserRepository userRepository;

    @Before
    public void set_Up() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void loadUserByUsername() {
        User user = new User();
        Role role = new Role();

        UserDetails userDetails1;
        UserDetails userDetails2;
        role.setName(RoleName.ROLE_USER);
        role.setId(1L);
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(role);
        user.setId(1L);
        user.setPassword("12345678");
        user.setEmail("oleg@gmail.com");
        user.setName("Oleg");
        user.setUsername("Oleg");
        user.setRoles(roleSet);
        Optional<User> optionalUser = Optional.of(user);


        when(userRepository.findByUsername(anyString())).thenReturn(optionalUser);

        userDetails1 = UserPrinciple.build(user);
        userDetails2 = userDetailsService.loadUserByUsername(anyString());
        assertEquals(userDetails1, userDetails2);

    }

    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByUsername_null() {
        Optional<User> userOptional = Optional.empty();
        when(userRepository.findByUsername(anyString())).thenReturn(userOptional);
        userDetailsService.loadUserByUsername(anyString());
    }

}